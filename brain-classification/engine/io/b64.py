"""
Conversion to base64 for frontend
"""
import io
import cv2
import base64
import numpy as np
from PIL import Image


def image_to_b64(image):
    """
    :param image: image to convert
    :return: b64 image
    """
    # img is a numpy array / opencv image
    _, encoded_img = cv2.imencode('.jpg', image)
    base64_img = base64.b64encode(encoded_img).decode('utf-8')
    return base64_img


def b64_to_img(image_string):
    """
    :param image_string: base64 image string
    :return: numpy image
    """
    img_data = base64.b64decode(image_string)
    image = Image.open(io.BytesIO(img_data))
    return cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)


if __name__ == "__main__":
    # test conversion
    img = cv2.imread('')
    b64_img = image_to_b64(img)
    img = b64_to_img(b64_img)

