"""
DICOM IO
"""
import tempfile
import numpy as np
import cv2
import os
import SimpleITK as sitk
from base64 import b64decode
import time


def downsample_patient(dicom_image, size):
    """
    Downsample a dicom image to a desirable size.
    :param dicom_image: dicom image to be processed
    :param size: size to downsample to
    :return: downsampled image
    """
    dimension = dicom_image.GetDimension()
    reference_physical_size = np.zeros(dicom_image.GetDimension())
    reference_physical_size[:] = [(sz - 1) * spc if sz * spc > mx else mx for sz, spc, mx in
                                  zip(dicom_image.GetSize(), dicom_image.GetSpacing(), reference_physical_size)]
    reference_origin = dicom_image.GetOrigin()
    reference_direction = dicom_image.GetDirection()
    reference_size = size
    reference_spacing = [phys_sz / (sz - 1) for sz, phys_sz in zip(reference_size, reference_physical_size)]
    reference_image = sitk.Image(reference_size, dicom_image.GetPixelIDValue())
    reference_image.SetOrigin(reference_origin)
    reference_image.SetSpacing(reference_spacing)
    reference_image.SetDirection(reference_direction)
    reference_center = np.array(
        reference_image.TransformContinuousIndexToPhysicalPoint(np.array(reference_image.GetSize()) / 2.0))
    transform = sitk.AffineTransform(dimension)
    transform.SetMatrix(dicom_image.GetDirection())
    transform.SetTranslation(np.array(dicom_image.GetOrigin()) - reference_origin)
    centering_transform = sitk.TranslationTransform(dimension)
    img_center = np.array(
        dicom_image.TransformContinuousIndexToPhysicalPoint(np.array(dicom_image.GetSize()) / 2.0))
    centering_transform.SetOffset(np.array(transform.GetInverse().TransformPoint(img_center) - reference_center))
    centered_transform = sitk.CompositeTransform([transform, centering_transform])
    resampled_img = sitk.Resample(dicom_image, reference_image, centered_transform, sitk.sitkLinear, 0.0)
    return resampled_img


def get_pixels_hu(path):
    """
    Hunsfield scaling for images
    :param path: path to the dicom image
    :return: scaled image
    """
    image = sitk.ReadImage(path, sitk.sitkInt32)
    image = np.array(sitk.GetArrayFromImage(image), dtype=np.float32)
    image = np.moveaxis(image, 0, 2)
    image = sitk.GetImageFromArray(image, sitk.sitkInt32)
    image = downsample_patient(image, (224, 224))
    image = sitk.GetArrayFromImage(sitk.Cast(sitk.RescaleIntensity(image), sitk.sitkVectorUInt8))
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    return image


def get_input_array_from_dicom_file(path):
    """
    :param path: dicom file path
    :return: loaded image
    """
    image_total = get_pixels_hu(path)
    return image_total


def get_input_array_from_dicom_bytes(data):
    """
    :param data: dicom data
    :return: loaded image
    """
    with tempfile.NamedTemporaryFile(suffix='.dcm') as f:
        f.write(data.read())
        image_total = get_pixels_hu(f.name)
    return image_total


def get_input_array_from_dicom_b64(dicom_b64):
    """
    :param dicom_b64: dicom b64 data
    :return: loaded image
    """
    with tempfile.NamedTemporaryFile(mode='wb', suffix='.dcm') as f:
        f.write(b64decode(dicom_b64))
        image_total = get_pixels_hu(f.name)
    return image_total

