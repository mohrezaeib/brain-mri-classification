import cv2
import numpy as np
from typing import Dict, Tuple


def draw_labels(image, labels: Dict, size: Tuple = (512, 512), show: bool = True,
                bordersize: int = 25, padding: int = 20):
    """
    Draws labels on an image
    :param image: image to draw the labels on.
    :param labels: dict like labels
    :param size: output image size
    :param show: popup window to show image if True.
    :param bordersize: pixel per label to add above the image
    :param padding: distance between labels, should be close to bordersize so wont write on the image.
    :return: image with labels drawn on it
    """
    image = cv2.resize(image, size)
    # top, bottom, left, right
    image = cv2.copyMakeBorder(image, len(labels) * bordersize, 0, 0, 0, cv2.BORDER_CONSTANT)
    for i, (key, value) in enumerate(labels.items()):
        cv2.putText(image, f'{key}: {np.around((value*100), 2)}%', (20, ((i + 1) * padding)), cv2.FONT_HERSHEY_SIMPLEX,
                    0.75, (0, 0, 255), 1, cv2.LINE_AA)
    if show:
        cv2.imshow('image', image)
        cv2.waitKey(0)
    return image


def concat_images(img1, img2, channels=3, concat_type='horizontal'):
    """
    :param img1: first image
    :param img2: second image
    :param channels: number of channels to output
    :param concat_type: (Optional) type of concat to do
    :return: horizontally/vertically attached image
    """
    if channels not in [1, 3]:
        raise ValueError('channels can be either 1 or 3!')
    if channels == 3:
        if img1.shape[-1] != 3:
            img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)
        if img2.shape[-1] != 3:
            img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)
    else:
        if img1.shape[-1] == 3:
            img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
        if img2.shape[-1] == 3:
            img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    if concat_type == 'horizontal':
        img_h = cv2.hconcat([img1, img2])
    elif concat_type == 'vertical':
        img_h = cv2.vconcat([img1, img2])
    else:
        raise ValueError('concat_type should be either horizontal or vertical')
    return img_h


if __name__ == '__main__':
    # test
    img = cv2.imread('/home/parsa/Downloads/covid-detection.jpg')
    # dummy labels
    lab = {'covid-detection': 0.50, 'normal': 0.10, 'lung opacity': 0.15, 'pneumonia': 0.25}
    draw_labels(img, lab, (512, 512), True)

