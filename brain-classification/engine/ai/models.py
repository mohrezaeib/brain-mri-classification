import torch
from torch import nn
import torchvision.transforms.functional as TF
from torchvision import datasets, models, transforms
import albumentations
import albumentations.pytorch
import numpy as np
from pytorch_grad_cam import GradCAM

class MRIClassficationResnet18(nn.Module):
    """ initial block for residual unet """

    def __init__(self,weight_path,device, transformer=None):
        super(MRIClassficationResnet18, self).__init__()

        model=  models.resnet18(pretrained=False)
        num_ftrs = model.fc.in_features    
        model.fc = nn.Linear(num_ftrs, 2)
        self.network = model
        if  weight_path is not None:
            self.network.load_state_dict(torch.load(weight_path, map_location=device)) 

        self. transformer =transformer

    def forward(self, x):
        return self.network(x)
        

    def preprocess(self, image):
      
        image = np.asanyarray(image)
        if self.transformer is not None:
            image = self.transformer(image=image)["image"]
        image = image.float()
        image = (image - image.min())/(image.max()+ 1e-5)
        return torch.cat([image,image,image])
    def predict(self, images):
        """
        Use the loaded model to make an estimation.
        :param images: photo to make the prediction on.
        :return: predicted class probability.
        """
        self.network.eval()
        preds = self.network(images)
        preds = preds.argmax(dim=1).data.cpu().numpy()
        return preds

    def predict_one(self, image):
        """
        Use the loaded model to make an estimation.
        :param images: photo to make the prediction on.
        :return: predicted class probability.
        """
        self.network.eval()
        image = torch.unsqueeze(image, dim=0)
        print(image.shape)
        preds = self.network(image)
        preds = preds.argmax(dim=1).data.cpu().numpy()[0]
        return preds



    def make_cam(self, image):
        self.network.eval()
        target_layers = [self.network.layer4[-1]]
        cam = GradCAM(model=self.network, target_layers=target_layers, use_cuda=False)
        grayscale_cams = cam(input_tensor=torch.unsqueeze(image,0),eigen_smooth=True,aug_smooth=True)
        return grayscale_cams[0]


def load_model(device, weight_path=None, transform=None):
    """ create model from class ,load weights and place them to device """
    model = MRIClassficationResnet18(weight_path, device, transform).to(device)


    return model
