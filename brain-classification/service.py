from flask import Flask
from flask_restful import Api
from service_warmup import init, APP_NAME

# flask settings
app = Flask (APP_NAME)
# flask rest initiation
api = Api (app)

# globals
app = init (app)