from endpoints import BratsMriSegmentation
from service import api, app

# could register others here as easily once they are added to endpoints.py
api.add_resource(BratsMriSegmentation, app.config['ENDPOINT'])

if __name__ == '__main__':
    # app.run (host='0.0.0.0', port=8080, debug = True)
    app.run("127.0.0.1", port=3000)
