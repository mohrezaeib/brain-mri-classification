# FORM
curl -v http://0.0.0.0:5000/brain-mri-classification -F file=@../../files.zip



# JSON
(echo -n '{"file": "'; base64 ../../files.zip; echo '"}') | curl -v -H "Content-Type: application/json" -d @-  http://0.0.0.0:5000/brain-mri-classification
