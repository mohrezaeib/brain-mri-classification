#!/usr/bin/env python
import os
import io
import re
from setuptools import setup, find_packages


def parse_requirements (filepath):
    with open (filepath) as f:
        contents = f.read ()
    return contents.split ('\n')


def read (*names, **kwargs):
    with io.open(os.path.join(os.path.dirname (__file__), *names),
                 encoding=kwargs.get ("encoding", "utf8")) as fp:
        return fp.read ()


def find_version (*file_paths):
    version_file = read (*file_paths)
    version_match = re.search (r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError ("Unable to find version string.")


try:
    import pypandoc
    long_description = pypandoc.convert('README.md', 'rst')
except (IOError, ImportError):
    long_description = open('README.md').read()

setup(
    # Metadata
    name='engine',
    version='0.0.1',
    author='Milad Hasani',
    description='Flask API backend',
    long_description = long_description,
    license='Apache-2.0',
    packages = find_packages(),
    # Package info
    # include_package_data=True,
    install_requires = parse_requirements('requirements.txt'),
)
