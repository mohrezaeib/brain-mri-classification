from glob import glob
import cv2
from flask import jsonify, make_response
from flask_restful import Resource
from tempfile import TemporaryDirectory
from torch.utils.data import DataLoader
from service_warmup import DEVICE
from service import app, api
from engine.io.b64 import image_to_b64, b64_to_img
from engine.io.dicom import get_input_array_from_dicom_bytes, get_input_array_from_dicom_b64
from engine.vis.drawer import draw_labels, concat_images
from service import app
from PIL import Image
from io import BytesIO
from base64 import b64decode
import imghdr
import cv2
import numpy as np

from PIL import Image

def _build_cors_preflight_response():
    response = make_response()
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    response.headers.add('Access-Control-Allow-Methods', "*")
    return response


# rest endpoints
class BratsMriSegmentation(Resource):
    """
    tumor Detection Model Endpoint
    """
    def options(self):
        return _build_cors_preflight_response()

    def post(self):
        """
        Post Method for the endpoint, since its rest there won't be a get method
        :return: base64 image encoded in a json as "image".
        """
        # get the json data
        args = app.config['PARSER'].parse_args()
        contents = args['file']
        try:
            # get image extension here
            ext = contents.filename.split('.')[-1]
            # check if the extension is readable
            if ext not in app.config['ALLOWED_EXTENSIONS']:
                error_code = {'status': 'could not recognize image format!'}
                return jsonify(error_code)
        except AttributeError:
            # means base64 is being posted
            # TODO: not the best way here needs optimization.
            decoded_string = b64decode(contents)
            ext = imghdr.what(None, h=decoded_string)
            if ext is None:
                # then is probs dicom but wont make that much sense rly.
                # try to write it if the file is not dicom throw error at simpleitk
                ext = 'dcm'
        # JSON
        if app.config['POST_TYPE'] == 'JSON':
            if ext == 'dcm':
                # try to read the data without ext if there are errors then its not dicom
                try:
                    image = get_input_array_from_dicom_b64(contents)
                except RuntimeError:
                    # not dicom content
                    error_code = {'status': 'could not recognize image format!'}
                    return jsonify(error_code)
                if image.shape[-1] == 3:
                    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            else:
                image = b64_to_img(contents)
                image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # FORM HERE
        else:
            if ext == 'dcm':
                # simpleitk has no byte reading? pydicom does but that is known to be bad.
                image = get_input_array_from_dicom_bytes(contents)
                if image.shape[-1] == 3:
                    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            else:
                # JPEG, PNG.
                image = np.array(Image.open(BytesIO(contents.read())).convert('L'))
                # if image.shape[-1] == 3:
                #     image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

        print('[INFO] processing request...')
        original_image = image
        image = app.config['MODEL'].preprocess(image)
        # predict with the model and get the grad cam
        prediction = app.config['MODEL'].predict_one(image)
        gradcam = app.config['MODEL'].make_cam(image)
        # gradcam = cv2.cvtColor(gradcam, cv2.COLOR_BGR2RGB)
        # create the label dict required by draw_labels
        label_dict = {app.config['PRED_DICT'][prediction]}
        image = np.transpose(image.data.cpu().numpy(), (1,2,0))
        image = (image*255).astype(np.uint8)
        gradcam = (gradcam*255).astype(np.uint8)

        # concatenate the images for better display
        final_image = concat_images(img1=image, img2=gradcam, channels=3, concat_type='horizontal')
        # draw the labels
        # don't draw the labels since the design is so good
        # final_image = draw_labels(image=final_image, labels=label_dict, size=(512, 284), show=False)
        cv2.imwrite('result.jpg', final_image)
        # this is for debugging cam output before conversion
        b64_res = image_to_b64(final_image)
        # since dcm is read by simpleitk and is preprocessed its no longer dcm
        # add the response format to the result for frontend
        result = f"data:image/jpg;base64" + ',' + b64_res
        # add the label dictionary for front as nested JSON
        output_dict = str({'result': result, 'labels': label_dict}) 
        return jsonify(output_dict)

