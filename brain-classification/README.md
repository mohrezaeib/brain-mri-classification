# MultiLabel BraTs MRI Segmentation

## Usage

**install the engine**

```bash
python setup.py install
```
**run the application**

```bash
chmod +x run.sh
./run.sh
```
`for extra options refer to service_warmup.py`

### Example of POSTING
 
**JSON**

```bash
(echo -n '{"file": "'; base64 path/to/files.zip; echo '"}') | curl -v -H "Content-Type: application/json" -d @-  http://IP_ADDR:5000/brain-mri-classification
```

**FORM**

```bash
curl -v http://IP_ADDR/brain-mri-classification -F file=@path/to/files.zip
```