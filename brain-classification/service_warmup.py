import os
from multiprocessing import Pool

import torch
from flask import Flask
from flask_restful import reqparse
from werkzeug.datastructures import FileStorage

from albumentations.pytorch import ToTensorV2
import albumentations
import albumentations.pytorch
from engine.ai.models import load_model

from dotenv import load_dotenv

load_dotenv()

APP_NAME = os.getenv('APP_NAME', 'brain-mri-classification')
MODEL_PATH = os.getenv('MODEL_NAME', '/home/mohre/D/medic/brain-mri-classification/brain-classification/weights/brain_mri_classifier_2d_v3.pth')
POST_TYPE = os.getenv('POST_TYPE', 'FORM')  # 'FORM' or 'JSON'
ENDPOINT = os.getenv('ENDPOINT', '/brain-mri-classification')
PRED_DICT = {0: 'normal',
             1: 'abnormal'}
             
ALLOWED_EXTENSIONS = ['png','jpg','dcm']
DEVICE = os.getenv('DEVICE', torch.device('cuda' if torch.cuda.is_available() else 'cpu'))
BATCH_SIZE = int(os.getenv("BATCH_SIZE", 4))
POOL_WORKERS = int(os.getenv("POOL_WORKERS", 4))

TRANSFORMS = albumentations.Compose([
                            albumentations.Resize(256, 256), 
                            albumentations.CenterCrop(224, 224),
                            albumentations.pytorch.ToTensorV2()
                        ])


def init(application: Flask) -> Flask:
    application.config['POST_TYPE'] = POST_TYPE
    application.config['ENDPOINT'] = ENDPOINT
    application.config['PRED_DICT'] = PRED_DICT
    application.config['MODEL_PATH'] = MODEL_PATH
    application.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS
    application.config['DEVICE'] = DEVICE
    application.config['TRANSFORMS'] = TRANSFORMS
    application.config['pool'] = Pool(processes=POOL_WORKERS)
    application.config['MODEL'] = load_model(
        device=application.config['DEVICE'],
        weight_path=application.config['MODEL_PATH'],transform=  application.config['TRANSFORMS'])
    application.config['bs'] = BATCH_SIZE
    application.config['PARSER'] = reqparse.RequestParser()
    # json type
    if application.config['POST_TYPE'] == 'JSON':
        application.config['PARSER'].add_argument('file',
                                                  required=True,
                                                  help='provide an 3D images as base64')
    else:
        # go for forms
        application.config['PARSER'].add_argument('file',
                                                  type=FileStorage,
                                                  location='files',
                                                  required=True,
                                                  help='provide a file')

    return application
